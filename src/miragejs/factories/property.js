import { randomNumber } from './utils';
import { Factory } from 'miragejs';
import faker from 'faker';

export default {
  property: Factory.extend({
    new() {
      return faker.fake('{{random.boolean}}');
    },
    beds() {
      return randomNumber(4);
    },
    baths() {
      return randomNumber(3);
    },
    title() {
      return faker.fake('{{lorem.words}}');
    },
    formattedPrice() {
      return faker.fake('{{finance.amount}}');
    },
    // ratting() {
    //   return randomNumber(5);
    // },
    // reviewCount() {
    //   return randomNumber(100);
    // },
    afterCreate(property, server) {
      const reviews = server.createList('review', randomNumber(45), { property });
      const rattingSum = reviews.reduce((acc, review) => acc + review.ratting, 0);
      const ratting = Math.ceil(rattingSum / reviews.length);
      const reviewCount = reviews.length;
      property.update({ reviewCount, ratting });
    },
  }),
};
