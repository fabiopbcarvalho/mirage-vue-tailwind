import { randomNumber } from './utils';
import { Factory } from 'miragejs';
import faker from 'faker';

export default {
  review: Factory.extend({
    content() {
      return faker.fake('{{lorem.paragraph}}');
    },
    user() {
      return faker.fake('{{name.findName}}');
    },
    ratting() {
      return randomNumber(5);
    },
    date() {
      const date = new Date(faker.fake('{{date.past}}'));
      return date.toLocaleDateString();
    },
  }),
};
