import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { filtersBase } from './utils';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    properties: [],
    reviews: {},
    filters: {},
  },
  mutations: {
    ['SET_PROPERTIES'](state, { properties }) {
      state.properties = properties;
    },
    ['SET_REVIEWS'](state, { propertyId, reviews }) {
      // state.reviews[propertyId] = reviews;
      Vue.set(state.reviews, propertyId, reviews);
    },
    ['RESET_REVIEWS'](state, { propertyId }) {
      if (propertyId) {
        Vue.set(state.reviews, propertyId, []);
      } else {
        Vue.set(state, 'reviews', {});
      }
    },
    ['SET_FILTER'](state, { item, value }) {
      if (!state.filters[item]) {
        Vue.set(state.filters, item, []);
      }
      if (state.filters[item].includes(value)) {
        state.filters[item] = state.filters[item].filter(itens => itens !== value);
      } else {
        state.filters[item].push(value);
      }
    },
  },
  actions: {
    fetchProperties({ commit }) {
      return axios.get(`api/properties`).then(res => {
        commit('SET_PROPERTIES', { properties: res.data.properties });
      });
    },
    fetchReviews({ commit }, propertyId) {
      return axios.get(`api/reviews?propertyId=${propertyId}`).then(res => {
        commit('SET_REVIEWS', { propertyId, reviews: res.data.reviews });
      });
    },
    resetReviews({ commit }, propertyId = null) {
      commit('RESET_REVIEWS', { propertyId });
    },
    setFilter({ commit }, { item, value }) {
      commit('SET_FILTER', { item, value });
    },
  },
  getters: {
    filters({ properties }) {
      return filtersBase(properties);
    },
    properties({ properties, filters }) {
      if (filters.beds && filters.beds.length > 0) {
        properties = properties.filter(property => filters.beds.includes(property.beds));
      }
      return properties;
    },
  },
  // modules: {},
});
