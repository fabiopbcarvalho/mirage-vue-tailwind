export const filtersBase = properties => {
  const itens = ['beds', 'baths', 'ratting'];
  return itens.reduce((acc, item) => {
    const all = properties.map(p => p[item]);
    const uniq = [...new Set(all)];
    acc[item] = uniq.sort();
    return acc;
  }, {});
};
